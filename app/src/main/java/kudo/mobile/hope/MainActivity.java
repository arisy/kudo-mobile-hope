package kudo.mobile.hope;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.root_view)
    View mRootView;

    @ViewById(R.id.recycler_view)
    RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private List<Hope> hopes;
    private HopeAdapter hopeAdapter;

    private DatabaseReference mDbHopes;
    private FirebaseDatabase mFirebaseInstance;
    private DatabaseReference mDbUsers;

    @AfterViews
    void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            HopeInputDialogFragment dialogFragment = HopeInputDialogFragment_.builder().build();
            dialogFragment.setListener(hope -> {
                Snackbar.make(mRootView, hope, Snackbar.LENGTH_LONG).show();
                createHope(hope);
            });
            dialogFragment.show(getSupportFragmentManager(), "TAG_NIH");
        });

        if (mRecyclerView != null) {
            mRecyclerView.setHasFixedSize(true);
        }

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        hopes = new ArrayList<>();
        hopeAdapter = new HopeAdapter(hopes);
        mRecyclerView.setAdapter(hopeAdapter);

        mFirebaseInstance = FirebaseDatabase.getInstance();
        mDbHopes = mFirebaseInstance.getReference("hopes");
        mDbUsers = mFirebaseInstance.getReference("users");

        retrieveLikes();
    }

    String userId;
    private void createHope(String hopeText) {
        userId = FirebaseAuth.getInstance().getUid();

        Hope hope = new Hope();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            hope.name = currentUser.getDisplayName();
        }
        hope.hope = hopeText;
        hope.timestamp = System.currentTimeMillis();
        hope.likes = 0;

        mDbHopes.push().setValue(hope);
    }

    private void setValueEventListener() {
        mDbHopes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                int oldSize = hopes.size();
                ArrayList<Hope> oldArray = new ArrayList();
                oldArray.addAll(hopes);
                hopes.clear();
                for (DataSnapshot postSnapshot: snapshot.getChildren()) {
                    Hope value = postSnapshot.getValue(Hope.class);
                    value.id = postSnapshot.getKey();
                    hopes.add(value);
                }
                Collections.sort(hopes);
                for (int i = 0; i < oldArray.size(); i++) {
                    if (!hopes.get(i).id.equals(oldArray.get(i).id) || hopes.get(i).likes != oldArray.get(i).likes) {
                        hopeAdapter.notifyItemChanged(i);
                    }
                }
                if (oldSize > hopes.size()) {
                    hopeAdapter.notifyItemRangeRemoved(oldSize - 1, oldSize - hopes.size());
                } else if (oldSize < hopes.size()) {
                    hopeAdapter.notifyItemRangeInserted(hopes.size(), hopes.size() - oldSize);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void retrieveLikes() {
        mDbUsers.child(FirebaseAuth.getInstance().getUid()).child("liked_post").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<String> value = dataSnapshot.getValue(new GenericTypeIndicator<List<String>>(){});
                if (value != null) {
                    hopeAdapter.setLikedPost(value);
                }
                setValueEventListener();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
