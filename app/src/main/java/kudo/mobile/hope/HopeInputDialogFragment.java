package kudo.mobile.hope;

import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by hendra on 25/01/18.
 */

@EFragment(R.layout.fragment_hope_input)
public class HopeInputDialogFragment extends NonTitleDialogFragment {

    private Listener mListener;

    @ViewById(R.id.submit_btn)
    Button mSubmitButton;

    @ViewById(R.id.message_input)
    EditText mHopeEditText;

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        }
        super.onResume();
    }

    @AfterViews
    void init() {
        mSubmitButton.setOnClickListener(view -> {
            if (mListener != null) {
                String hope = mHopeEditText.getText().toString();
                if (!TextUtils.isEmpty(hope)) {
                    mListener.onSubmit(hope);
                    dismiss();
                }
            }
        });
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {

        void onSubmit(String hope);

    }
}
