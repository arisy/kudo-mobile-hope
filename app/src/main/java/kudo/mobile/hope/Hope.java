package kudo.mobile.hope;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Hope implements Comparable<Hope> {

    @Exclude
    public String id;
    public String name;
    public String hope;
    public long timestamp;
    public int likes;

    // Default constructor required for calls to
    // DataSnapshot.getValue(Hope.class)
    public Hope() {
    }

    public Hope(String name, String hope, long timestamp, int likes) {
        this.name = name;
        this.hope = hope;
        this.timestamp = timestamp;
        this.likes = likes;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return ((Hope)obj).id.equals(id);
    }

    @Override
    public int compareTo(@NonNull Hope hope) {
        return hope.likes - likes;
    }
}
