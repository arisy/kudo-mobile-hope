package kudo.mobile.hope;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.KeyEvent;
import android.view.Window;

public class NonTitleDialogFragment extends AppCompatDialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Window windowDialog = dialog.getWindow();
        if (windowDialog != null) {
            windowDialog.requestFeature(Window.FEATURE_NO_TITLE);
        }
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int code, KeyEvent event) {
                if(code == KeyEvent.KEYCODE_BACK){
                }
                return false;
            }
        });
        return dialog;
    }
}