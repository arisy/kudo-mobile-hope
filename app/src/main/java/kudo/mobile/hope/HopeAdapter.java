package kudo.mobile.hope;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

public class HopeAdapter extends RecyclerView.Adapter<HopeAdapter.ViewHolder> {

    private Collection<String> likedHope;
    private final DatabaseReference mDbHopes;
    private final DatabaseReference mDbUsers;
    private List<Hope> songList;

    public void setLikedPost(List<String> likedPost) {
        this.likedHope = likedPost;
    }

    //Provide a reference to the views for each data item
    //Complex data items may need more than one view per item, and
    //you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //each data item is just a string in this case
        public TextView tvName, tvHope, tvLikes, tvDate;
        public ImageButton btnLike;

        public ViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tv_name);
            tvHope = v.findViewById(R.id.tv_hope);
            tvDate = v.findViewById(R.id.tv_date);
            tvLikes = v.findViewById(R.id.tv_likes);
            btnLike = v.findViewById(R.id.btn_like);

        }
    }

    //Provide a suitable constructor
    public HopeAdapter(List<Hope> songList) {
        this.songList = songList;
        this.likedHope = new HashSet<>();

        FirebaseDatabase mFirebaseInstance = FirebaseDatabase.getInstance();
        mDbHopes = mFirebaseInstance.getReference("hopes");
        mDbUsers = mFirebaseInstance.getReference("users");
    }

    //Create new views (invoked by the layout manager)
    @Override
    public HopeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //Creating a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.hope_list_item, parent, false);

        //set the view's size, margins, paddings and layout parameters

        return new ViewHolder(v);
    }

    //Replace the contents of a view (invoked by the layout manager
    @Override
    public void onBindViewHolder(HopeAdapter.ViewHolder holder, int position) {

        // - get element from arraylist at this position
        // - replace the contents of the view with that element

        Hope song = songList.get(position);
        holder.tvName.setText(String.valueOf(song.name));
        holder.tvHope.setText(song.hope);
        holder.tvDate.setText(new SimpleDateFormat("dd-MM-yyyy").format(new Date(song.timestamp)));
        holder.tvLikes.setText(song.likes + " likes");
        if (likedHope.contains(song.id)) {
            holder.btnLike.setImageResource(R.drawable.ic_favorite_red_700_18dp);
        } else {
            holder.btnLike.setImageResource(R.drawable.ic_favorite_empty);
        }
        holder.btnLike.setOnClickListener(view -> {
            if (likedHope.contains(song.id)) {
                removeLike(song);
                holder.btnLike.setImageResource(R.drawable.ic_favorite_empty);
            } else  {
                addLike(song);
                holder.btnLike.setImageResource(R.drawable.ic_favorite_red_700_18dp);
            }
        });
    }

    private void removeLike(Hope hope) {
        Map<String, Object> likeMap = new HashMap<>();
        likeMap.put("likes", hope.likes - 1);
        mDbHopes.child(hope.id).updateChildren(likeMap);
        likedHope.remove(hope.id);
        Map<String, Object> likeList = new HashMap<>();
        likeList.put("liked_post", Arrays.asList(likedHope.toArray()));
        mDbUsers.child(FirebaseAuth.getInstance().getUid()).updateChildren(likeList);
    }

    private void addLike(Hope hope) {
        Map<String, Object> likeMap = new HashMap<>();
        likeMap.put("likes", hope.likes + 1);
        mDbHopes.child(hope.id).updateChildren(likeMap);
        likedHope.add(hope.id);
        Map<String, Object> likeList = new HashMap<>();
        likeList.put("liked_post", Arrays.asList(likedHope.toArray()));
        mDbUsers.child(FirebaseAuth.getInstance().getUid()).updateChildren(likeList);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}